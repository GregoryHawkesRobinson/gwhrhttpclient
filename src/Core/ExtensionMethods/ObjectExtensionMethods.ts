// Authors:  Gregory Hawkes-Robinson (ionstorm3@outlook.com)


declare global {
    interface ObjectConstructor {
        isNullOrUndefined(obj: any): boolean;
    }

    interface StringConstructor {
        isNullUndefinedOrEmpty(obj: string): boolean;

        empty: string;
    }
}


Object.isNullOrUndefined = (obj: any): boolean => {
    return typeof obj === "undefined" || obj === null
};
String.isNullUndefinedOrEmpty = (obj: string): boolean => {
    return typeof obj === "undefined" || obj === null || obj === "";
};
String.empty = "";
export {};