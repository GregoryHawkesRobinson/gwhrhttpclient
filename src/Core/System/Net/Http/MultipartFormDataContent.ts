// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

import {HttpContent} from "../../../../Core/System/Net/Http/HttpContent";
import {FileContent} from "../../../../Core/System/Net/Http/FileContent";

export class MultipartFormDataContent extends HttpContent {

    private _items: Map<string, HttpContent> = new Map<string, HttpContent>();

    public add(name: string, value: HttpContent): void {
        this._items.set(name, value);
    }

    public remove(name: string): void {
        this._items.delete(name);
    }

    public toPayload(): any {
        let formData: FormData = new FormData();
        this._items.forEach((value: HttpContent, key: string) => {
            if(value instanceof FileContent){
                let file:File = value.toPayload();
                formData.append(key,file,file.name);
                return;
            }
            formData.append(key,value.toPayload());
        });

        return formData;
    }
}