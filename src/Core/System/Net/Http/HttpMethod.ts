// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

export enum HttpMethod {
    Get="GET",
    Head="HEAD",
    Post="Post",
    Put="PUT",
    Delete="DELETE",
    Connect="CONNECT",
    Options="Options",
    Trace="TRACE",
    Patch="PATCH"
}