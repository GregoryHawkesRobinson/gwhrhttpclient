// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

export enum HttpRequestRedirect {
    Follow="follow",
    Error="error",
    Manual="manual"
}