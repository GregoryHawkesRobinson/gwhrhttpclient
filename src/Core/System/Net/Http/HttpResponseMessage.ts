// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

import {HttpContent} from "../../../../Core/System/Net/Http/HttpContent";
import {HttpHeaders} from "../../../../Core/System/Net/Http/Headers/HttpHeaders";
import {HttpRequestMessage} from "../../../../Core/System/Net/Http/HttpRequestMessage";
import {HttpHeader} from "../../../../Core/System/Net/Http/Headers/HttpHeader";

export class HttpResponseMessage {
    //Internal fields
    private readonly _response: Response;
    private readonly _requestMessage: HttpRequestMessage;
    private readonly _content: HttpContent;

    constructor(response: Response, requestMessage: HttpRequestMessage) {
        this._response = response;
        this._requestMessage = requestMessage;
        this._content = new HttpContent(response);
    }

    //#region Public properties

    public get content(): HttpContent {
        return this._content;
    }

    public headers: HttpHeaders = new HttpHeaders();

    public get isSuccessStatusCode(): boolean {
        return this._response.ok;
    }

    public get reasonPhrase(): string {
        return this._response.statusText;
    }

    public get requestMessage(): HttpRequestMessage {
        return this._requestMessage;
    }

    public get statusCode(): number {
        return this._response.status;
    }

    //#endregion

    //#region Private methods

    private parseHeaders(): void {
        this._response.headers.forEach((value: string, key: string) => {
            this.headers.add(new HttpHeader(key, [value]));
        });
    }

    //#endregion
}