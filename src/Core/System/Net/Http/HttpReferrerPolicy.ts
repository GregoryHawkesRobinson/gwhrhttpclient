// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

export enum HttpReferrerPolicy {
    None="",
    NoReferrer="no-referrer",
    NoReferrerWhenDowngrade="no-referrer-when-downgrade",
    OriginOnly="origin-only",
    OriginWhenCrossOrigin="origin-when-cross-origin",
    UnsafeUrl="unsafe-url"
}