// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

export enum HttpRequestCache{
    Default="default",
    NoStore="no-store",
    Reload="reload",
    NoCache="no-cache",
    ForceCache="force-cache",
    OnlyIfCached="only-if-cached"
}