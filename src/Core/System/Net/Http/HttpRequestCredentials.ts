// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

export enum HttpRequestCredentials {
    Omit="omit",
    SameOrigin="same-origin",
    Include="include"
}