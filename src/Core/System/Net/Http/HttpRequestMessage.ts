// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

import {HttpContent} from "../../../../Core/System/Net/Http/HttpContent";
import {HttpHeaders} from "../../../../Core/System/Net/Http/Headers/HttpHeaders";
import {HttpMethod} from "./HttpMethod";

export class HttpRequestMessage {

    //#region Public properties

    public content: HttpContent = new HttpContent();
    public headers: HttpHeaders = new HttpHeaders();
    public method: HttpMethod = HttpMethod.Get;
    public requestUri: string = String.empty;

    //#endregion
}