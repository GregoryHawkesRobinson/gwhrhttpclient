// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

export enum HttpRequestMode {
    Navigate="navigate",
    SameOrigin="same-origin",
    NoCors="no-cores",
    Cors="cors"
}