// Authors:  Gregory Hawkes-Robinson (ionstorm3@outlook.com)

import {Action1} from "../../../../../Core/System/Action";
import {HttpHeader} from "../../../../../Core/System/Net/Http/Headers/HttpHeader";

export class HttpHeaders {
    //Internal fields
    private _headers: Set<HttpHeader> = new Set<HttpHeader>();

    //#region Public methods

    public add(value:HttpHeader): void {
        this._headers.add(value);
    }

    public clear(): void {
        this._headers.clear();
    }

    public remove(value: HttpHeader): void {
        this._headers.delete(value);
    }

    public forEach(func:Action1<HttpHeader>): void {
        this._headers.forEach((value:HttpHeader) => {
            func(value);
        });
    }

    //#endregion
}