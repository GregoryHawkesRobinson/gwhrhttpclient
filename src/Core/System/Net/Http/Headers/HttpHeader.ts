// Authors:  Gregory Hawkes-Robinson (ionstorm3@outlook.com)

export class HttpHeader {
    constructor(nameParam: string, valuesParam: string[]) {
        this.name = nameParam;

        valuesParam.forEach((value: string) => {
            this.values.add(value);
        });
    }

    //#region Public properties

    public name: string;
    public values: Set<string> = new Set<string>();

    //#endregion

    //#region Public methods

    public toString(): string {
        let header: string = this.name + ": ";
        let valuesAry: string[] = [];

        this.values.forEach((value: string) => {
            valuesAry.push(value);
        });

        header += valuesAry.join(",");
        return header;
    }

    //#endregion

}