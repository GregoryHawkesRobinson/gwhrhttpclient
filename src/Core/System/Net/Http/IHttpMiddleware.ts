// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

import {Func2} from "../../../../Core/System/Func";
import {HttpResponseMessage} from "../../../../Core/System/Net/Http/HttpResponseMessage";
import {HttpClient} from "../../../../Core/System/Net/Http/HttpClient";
import {HttpRequestMessage} from "../../../../Core/System/Net/Http/HttpRequestMessage";

export interface IHttpMiddleware {
    interceptRequest: Func2<HttpRequestMessage, HttpClient, Promise<void>>;
    interceptResponse: Func2<HttpResponseMessage, HttpClient, Promise<void>>;
}