// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

import {HttpContent} from "../../../../Core/System/Net/Http/HttpContent";
import {HttpHeader} from "../../../../Core/System/Net/Http/Headers/HttpHeader";

export class JsonContent extends HttpContent {
    //Internal fields
    private readonly _value:any;
    constructor(value: any, response?: Response) {
        super(response);
        this._value = value;
        this.headers.add(new HttpHeader("Content-Type",["application/json"]))
    }

    //#region Public methods

    public async readAsJsonAsyncAsync(): Promise<any>{
        return Object.isNullOrUndefined(this._response)?this._value:await this._response!.json();
    }

    public async readAsStringAsync(): Promise<string>{
        return Object.isNullOrUndefined(this._response)?JSON.stringify(this._value):await this._response!.text();
    }

    public toPayload(): any {
        return JSON.stringify(this._value);
    }

    //#endregion
}