// Authors:  Gregory Hawkes-Robinson (ionstorm3@outlook.com)

import {HttpContent} from "../../../../Core/System/Net/Http/HttpContent";
import {KeyValuePair} from "../../../../Core/System/Collections/Generic/KeyValuePair";

export class QueryStringContent extends HttpContent {
    //Internal fields
    private _value: KeyValuePair<string, string>[] = [];

    constructor(value: KeyValuePair<string, string>[]) {
        super();
        this._value = value;
    }

    public toPayload() {
        let valuesArry: string[] = [];
        this._value.forEach((kvp: KeyValuePair<string, string>) => {
            valuesArry.push(encodeURIComponent(kvp.key) + "=" + encodeURIComponent(kvp.value));
        });

        // Combine the pairs into a single string and replace all %-encoded spaces to
        // the '+' character; matches the behaviour of browser form submissions.
        // urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');
        return valuesArry.join('&').replace(/%20/g, '+');
    }
}