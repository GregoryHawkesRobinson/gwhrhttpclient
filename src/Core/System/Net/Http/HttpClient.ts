// Authors:  Gregory Hawkes-Robinson (ionstorm3@outlook.com)

import {HttpHeaders} from "../../../../Core/System/Net/Http/Headers/HttpHeaders";
import {IHttpMiddleware} from "../../../../Core/System/Net/Http/IHttpMiddleware";
import {HttpRequestMessage} from "../../../../Core/System/Net/Http/HttpRequestMessage";
import {HttpResponseMessage} from "../../../../Core/System/Net/Http/HttpResponseMessage";
import {HttpHeader} from "../../../../Core/System/Net/Http/Headers/HttpHeader";
import {HttpMethod} from "./HttpMethod";
import {HttpContent} from "../../../../Core/System/Net/Http/HttpContent";
// import {Debug} from "../../../../Core/System/Diagnostics/Debug";
import {FormUrlEncodedContent} from "../../../../Core/System/Net/Http/FormUrlEncodedContent";
import {QueryStringContent} from "../../../../Core/System/Net/Http/QueryStringContent";

export interface IHttpClientInit {
    baseAddress?: string,
    timeOut?: number
}

export class HttpClient {

    constructor(param?: IHttpClientInit) {
        if (Object.isNullOrUndefined(param)) {
            return;
        }

        if (!Object.isNullOrUndefined(param!.baseAddress)) {
            this.baseAddress = param!.baseAddress!;
        }

        if (!Object.isNullOrUndefined(param!.timeOut)) {
            this.timeOut = param!.timeOut!;
        }
    }

    //#region Public properties

    //Gets or sets the base url for all requests made
    public baseAddress: string = String.empty;

    //Gets or sets the headers that should be send with each request
    public defaultRequestHeaders: HttpHeaders = new HttpHeaders();

    //Gets or sets the number of milliseconds before the request times out
    public timeOut: number = 100000;

    //#endregion

    //#region Static properties

    public static middleware: Map<string, IHttpMiddleware> = new Map<string, IHttpMiddleware>();

    //#endregion

    //#region Static methods

    public static useMiddleware(name: string, middleware: IHttpMiddleware): void {
        let item: IHttpMiddleware | undefined = this.middleware.get(name);
        if (Object.isNullOrUndefined(item)) {
            this.middleware.set(name, middleware);
        }
    }

    //#endregion

    //#region Public methods

    public deleteAsync = async (requestUrl: string): Promise<HttpResponseMessage> => {

        //Create the request object
        let request: HttpRequestMessage = new HttpRequestMessage();
        request.method = HttpMethod.Delete;
        request.requestUri = requestUrl;

        return await this.sendAsync(request);
    };

    public getAsync = async (requestUrl: string): Promise<HttpResponseMessage> => {

        //Create the request object
        let request: HttpRequestMessage = new HttpRequestMessage();
        request.method = HttpMethod.Get;
        request.requestUri = requestUrl;

        return await this.sendAsync(request);
    };

    public postAsync = async (requestUrl: string, content: HttpContent): Promise<HttpResponseMessage> => {

        //Create the request object
        let request: HttpRequestMessage = new HttpRequestMessage();
        request.method = HttpMethod.Post;
        request.requestUri = requestUrl;
        request.content = content;

        return await this.sendAsync(request);
    };

    public putAsync = async (requestUrl: string, content: HttpContent): Promise<HttpResponseMessage> => {

        //Create the request object
        let request: HttpRequestMessage = new HttpRequestMessage();
        request.method = HttpMethod.Put;
        request.requestUri = requestUrl;
        request.content = content;

        return await this.sendAsync(request);
    };

    public sendAsync = async (request: HttpRequestMessage): Promise<HttpResponseMessage> => {

        //Run the request middleware
        HttpClient.middleware.forEach(async (value: IHttpMiddleware, key: string, map: Map<string, IHttpMiddleware>) => {
            await value.interceptRequest(request, this);
        });

        //Send the request
        let requestInit: RequestInit;
        // Debug.WriteLine("GwhrHttpClient Request method:");
        // Debug.WriteLine(request.method);
        // Debug.WriteLine("Payload:  ");
        // Debug.WriteLine(request.content.toPayload());
        if (request.method == HttpMethod.Get || request.method == HttpMethod.Head) {
            requestInit = {
                headers: this.parseRequestHeaders(request),
                method: request.method,
                body: undefined
            };

            // Debug.WriteLine("Uploading without a body");
        }
        else {
            // Debug.WriteLine("Uploading with a body");
            requestInit = {
                body: request.content.toPayload(),
                headers: this.parseRequestHeaders(request),
                method: request.method,
            };
        }

        if (request.content instanceof FormUrlEncodedContent || request.content instanceof QueryStringContent) {
            // Debug.WriteLine("Detected form url encodeded content");
            request.requestUri = request.requestUri + "?" + request.content.toPayload();
        }

        try {
            let response: Response = await fetch(this.baseAddress + request.requestUri, requestInit);

            //Package the response
            let responseMessage: HttpResponseMessage = new HttpResponseMessage(response, request);

            //Run the response middleware
            HttpClient.middleware.forEach(async (value: IHttpMiddleware, key: string, map: Map<string, IHttpMiddleware>) => {
                await value.interceptResponse(responseMessage, this);
            });

            return responseMessage;
        }
        catch (e) {
            let responseMsg: HttpResponseMessage = new HttpResponseMessage(new Response(null, {
                status: 503,
                statusText: "Service Unavailable"
            }), request);

            //Run the response middleware
            HttpClient.middleware.forEach(async (value: IHttpMiddleware, key: string, map: Map<string, IHttpMiddleware>) => {
                await value.interceptResponse(responseMsg, this);
            });

            return responseMsg;
        }

    };

    //#endregion

    //#region Private methods


    private parseRequestHeaders = (request: HttpRequestMessage): Headers => {
        let headers: Headers = new Headers();

        //Parse the content headers
        request.content.headers.forEach((header: HttpHeader) => {
            header.values.forEach((headerValue: string) => {
                // Debug.WriteLine("Name:  " + header.name);
                // Debug.WriteLine("Value:  " + headerValue);
                headers.append(header.name, headerValue);
            });
        });

        //Parse the request headers
        request.headers.forEach((header: HttpHeader) => {
            header.values.forEach((headerValue: string) => {
                // Debug.WriteLine("Name:  " + header.name);
                // Debug.WriteLine("Value:  " + headerValue);
                headers.append(header.name, headerValue);
            });
        });

        return headers;
    }

    //#endregion


}