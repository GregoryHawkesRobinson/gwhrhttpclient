// Authors:  Gregory Hawkes-Robinson (ionstorm3@outlook.com)

import {HttpContent} from "../../../../Core/System/Net/Http/HttpContent";
import {HttpHeader} from "../../../../Core/System/Net/Http/Headers/HttpHeader";

export class FileContent extends HttpContent {

    //Internal fields
    private _value: File;

    public constructor( value: File,response?: Response) {
        super(response);
        this._value = value;
        this.headers.add(new HttpHeader("Content-Type",["multipart/form-data"]))

    }

    public toPayload(): any {
        return this._value;
    }
}