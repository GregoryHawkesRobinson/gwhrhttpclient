// Authors:  Gregory Hawkes-Robinson (ionstorm3@outlook.com)

import {HttpHeaders} from "../../../../Core/System/Net/Http/Headers/HttpHeaders";

export class HttpContent {
    //Internal fields
    protected _response?: Response;

    constructor(response?: Response) {
        this._response = response;
    }

    //#region Public properties

    public headers: HttpHeaders = new HttpHeaders();

    //#endregion

    //#region Abstract methods

    public async readAsArrayBufferAsync(): Promise<ArrayBuffer> {
        return Object.isNullOrUndefined(this._response) ? new ArrayBuffer(0) : await this._response!.arrayBuffer();
    }

    public async readAsBlobAsync(): Promise<Blob> {
        return Object.isNullOrUndefined(this._response) ? new Blob() : await this._response!.blob();
    }

    public async readAsFormDataAsync(): Promise<FormData>{
        return Object.isNullOrUndefined(this._response)? new FormData(): await this._response!.formData();
    }

    public async readAsJsonAsync(): Promise<any>{
        return Object.isNullOrUndefined(this._response)?{}:await this._response!.json();
    }

    public async readAsStringAsync(): Promise<string>{
        return Object.isNullOrUndefined(this._response)?"":await this._response!.text();
    }

    public toPayload(): any{
        return String.empty;//throw new NotImplementedException("This method must be implemented in a subclass");
    }

    //#endregion
}