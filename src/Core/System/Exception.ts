// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

import {KeyValuePair} from "../../Core/System/Collections/Generic/KeyValuePair";

export class Exception extends Error{
    constructor(message?:string){
        super(message);
    }

    //#region Public properties
    public Data:KeyValuePair<string,string> = new KeyValuePair<string, string>("","");
    public HelpLink:string="";
    public HResult:number=0;
}