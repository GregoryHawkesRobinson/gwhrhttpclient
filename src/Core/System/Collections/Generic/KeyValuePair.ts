// Authors:  Gregory Hawkes-Robinson (ionstorm3@outlook.com)

export class KeyValuePair<TKey, TValue>{
    //Internal fields
    private _key:TKey;
    private readonly _value:TValue;

    constructor(key:TKey, value:TValue){
        this._key = key;
        this._value = value;
    }

    //Public properties
    public get key():TKey{
        return this._key;
    }
    public get value():TValue{
        return this._value;
    }
}