// Authors:
// 	Gregory Hawkes-Robinson (ionstorm3@outlook.com)

import {Exception} from "../../Core/System/Exception";

export class NotImplementedException extends Exception{
    constructor(message:string="The method or operation is not implemented"){
        super(message);
    }
}